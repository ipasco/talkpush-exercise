class CandidatesController < ApplicationController

	OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE


	def create
	require "google_drive"
	require "json"
	require 'rest-client'

	# Creates a session. This will prompt the credential via command line for the
	# first time and save it to config.json file for later usages.
	# See this document to learn how to create config.json:
	# https://github.com/gimite/google-drive-ruby/blob/master/doc/authorization.md
	session = GoogleDrive.saved_session("config.json")

	# First worksheet of
	# https://docs.google.com/spreadsheets/d/1xkofJa5iI3AQE4yWEoHqMTQ1QQ-VDsfUDDwV96QQDVM/edit#gid=1713596535
	ws = session.spreadsheet_by_key("1xkofJa5iI3AQE4yWEoHqMTQ1QQ-VDsfUDDwV96QQDVM").worksheets[0]
	count = Spreadsheet.find(1).row_count
	
	# Dumps all cells.
		(count..ws.num_rows).each do |row|

       RestClient.post('https://my.talkpush.com/api/talkpush_services/campaigns/589caafca7723859b0fec1b0c073522a/campaign_invitations', 
        	{api_key: '48530ba23eef6b45ffbc95d7c20a60b9', 
        	api_secret: 'e2f724ba060f82ddf58923af494578a7', 
        	campaign_invitation: 
        		{first_name: ws[row, 2].to_s, 
        		last_name:  ws[row, 3].to_s,
        		user_phone_number: ws[row, 4],
        		email: ws[row, 5].to_s},
        		source: 'Google Form'}
        	.to_json, {content_type: :json, accept: :json})

		end
		@s = Spreadsheet.find(1)
		@s.update(row_count: ws.num_rows+1)
	end	 	

end
