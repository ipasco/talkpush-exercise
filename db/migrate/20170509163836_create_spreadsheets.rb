class CreateSpreadsheets < ActiveRecord::Migration[5.0]
  def change
    create_table :spreadsheets do |t|
    	t.integer :row_count
      t.timestamps
    end
  end
end
